import React, { useState } from 'react';
import { Button } from 'reactstrap';

function AllDataColumns(props) {
const [numActive, setNumActive] = useState(0)

  return (
    <span>
      {props.columnData.map(column => (
        <Button color='info'>
        <ul 
        style={{listStyleType: 'none'}}
        onClick={() => { setNumActive(handleColumnSelect(column, numActive, props));
        }}>
            <li>{column.descriptor}</li>
            {column.data.map(dataPoint =>(
              <li>
                {dataPoint}
              </li>
            ))}
            </ul>
        </Button>
      ))}
    </span>
  );
}

function handleColumnSelect (column, numActive, props) {
  if (column.selected === true) {
    column.selected = false
    props.handleColumnClick(column, false)
    return numActive - 1;
  } else if (numActive < 2) {
    column.selected = true;
    props.handleColumnClick(column, true)
    return numActive + 1
  } else {
    console.log('CANNOT SELECT MORE THAN 2, DESELECT NOW')
    console.log('CANNOT SELECT MORE THAN 2, DESELECT NOW')
    console.log('CANNOT SELECT MORE THAN 2, DESELECT NOW')
    return numActive
  }
}

export default AllDataColumns;
