import React, { useEffect, useRef } from 'react';
import * as d3 from 'd3';
import InteractiveScatter from './InteractiveScatter3-d3';

const D3blackbox = ({ x, y, render }) => {
  const refAnchor = useRef(null);

  useEffect(() => {
    render(d3.select(refAnchor.current))
  });

  return <g ref={refAnchor} transform={`translate(${x}, ${y})`} />;
}

function InteractiveScatter3() {
  return (
    <div className="App">
      <h1>Drag and drop start/end line points</h1>
      <div>Description: drag points to move line</div>
      <svg width='500' height='400'>
        <D3blackbox x={0} y={0} render={svg => InteractiveScatter(svg, 500, 400)} />
      </svg>
    </div>
  );
}

export default InteractiveScatter3;
