import React from 'react';
import './App.css';
import NavHeader from './components/NavHeader'
import BarScatterContainer from './components/BarScatterContainer'
import InteractiveScatter1 from './components/InteractiveScatter1'
import InteractiveScatter2 from './components/InteractiveScatter2'
import InteractiveScatter3 from './components/InteractiveScatter3'
import Registration from './components/Registration'
import CsvImporter from './components/CsvImporter'
import Logo from './images/inflection_logo_banner.png'
import Login from './components/Login'
import { useRoutes } from 'hookrouter'
import "@fortawesome/fontawesome-free/css/all.min.css";
import "bootstrap-css-only/css/bootstrap.min.css";
import "mdbreact/dist/css/mdb.css";
 
const routes = {
    '/login': () => <Login />,
    '/registration': () => <Registration />,
    '/bar-scatter': () => <BarScatterContainer />,
    '/interactive-scatter-1': () => <InteractiveScatter1 />,
    '/interactive-scatter-2': () => <InteractiveScatter2 />,
    '/interactive-scatter-3': () => <InteractiveScatter3 />,
    '/csv-importer': () => <CsvImporter />
};

function App() {
  const routeResult = useRoutes(routes);

  return (
    <div className="App">
      <NavHeader />
      <img src={Logo} alt="Logo" className='Logo' />
      { routeResult }
    </div>
  );
}

export default App;
